#include <iostream>
#include <vector>

#include "vec.h"


void chk(int x, int max){
  if(x >= max){
    printf("Error at %d where max is %d",x,max);
    exit(-1);
  }
}
  


template<typename T>
T map_range(T value, T max_old, T max_new){
  return (value*max_new/max_old);
}

struct pixel{
  unsigned short int r,g,b;
  pixel(int red, int green, int blue){
    this->r = static_cast<int>(red);
    this->g = static_cast<int>(green);
    this->b = static_cast<int>(blue);
  }
  pixel(){
    r = g = b = 0;
  }
  pixel& operator=(const vec3& v){
    this->r = v.x;
    this->g = v.y;
    this->b = v.z;
    return *this;
  }
};


class Ray{
public:
  vec3 direction;
  vec3 origin;
  vec3 at(float t){
    return (origin + t*direction);  
  }
};



bool hit_sphere(const point3& center, double radius, const Ray& r) {
    vec3 oc = r.origin - center;
    auto a = dot(r.direction, r.direction);
    auto b = 2.0 * dot(oc, r.direction);
    auto c = dot(oc, oc) - radius*radius;
    auto discriminant = b*b - 4*a*c;
    return (discriminant > 0);
}



color ray_color(const Ray& r){
  if(hit_sphere(point3(0,0,-0.1),0.1,r)){
    return vec3(1,0,1);
  }
  vec3 unit_dir = unit_vector(r.direction);
  auto t = unit_dir.y + 1.0;
  return (1.0-t)*color(1.0, 1.0, 1.0) + t*color(0.5, 0.7, 1.0);
}

struct Aspect{
  float x;
  float y;
  float ratio() const{
    return x/y;
  }
};

int main(){
  //Write Image
  const int factor = 60;
  const Aspect asp = {16,9};

  const int image_width = factor*asp.x;
  const int image_height= factor*asp.y;

  pixel *image = (pixel *)malloc(image_width*image_height*sizeof(pixel));


  const float vp_height = 2.0;
  const float vp_width = asp.ratio()*vp_height;
  const float focal_length = 1.0;  
  point3 origin(0,0,0);

  point3 horizontal = point3(vp_width,0,0);
  point3 vertical   = point3(0,vp_height,0);
  point3 lower_left_corner = origin - horizontal/2 - vertical/2 - vec3(0,0,focal_length);


  
  for(int i = image_height -1; i >= 0; i--){
    for(int j = 0 ; j < image_width; j++){
      
      long int id = (image_height-(i+1))*image_width+j;

      chk(id,image_width*image_height);
      
      auto u = double(j) / (image_width-1);
      auto v = double(i) / (image_height-1);
      Ray r;
      r.origin = origin;
      r.direction =  lower_left_corner + u*horizontal + v*vertical - origin;
      color pc = ray_color(r)*256;
      image[id] = pixel(pc.x,pc.y,pc.z);
    }
  } 

  //Open a file
  FILE* out = fopen("./res.ppm","w");
  fprintf(out,"P3\n%d %d\n255\n",image_width,image_height);

  //Write the image
  for(int i = 0; i < image_height; i++){
    for(int j = 0; j < image_width; j++){

      long int id = i*image_width+j;
      chk(id,image_width*image_height);
      
      pixel* t = &image[id];
      fprintf(out, "%d %d %d ",t->r, t->g, t->b);
    }
    fprintf(out,"\n");
  }

  //Close the file
  free(image);
  fclose(out);
}